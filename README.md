# About This Repository
In this repository you will find some of the custom solutions that I have put together for client tagging requests. Where possible I've tried to provide a bit of background behind each of the requests so that the code would make a bit more sense.

Please note that in all cases the code was being delivered by a tag manager, the code would rarely be running in isolation, the syntax would be interpreted by the tag manager before deployment and certain objects and global variables would be present on the pages where this code would be designed to run.

I've made attempts to anonymise parts of the code that would indicate who the client is, but the folder breakdown should give an idea of the type of industry the client was working in. I've left vendor specific code as is since I don't think there is any need to alter that.

All code in this repo is available to the public via client websites if you know where to look.
