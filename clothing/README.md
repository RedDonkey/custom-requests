## Problem
The client's problem was that they were using a single account to serve different Criteo campaign tags across multiple website regions. Their architecture was such that they had three campaigns set up for their Criteo tagging but a multitude of "regional" websites. Therefore, they needed a way to somehow choose which of their three Criteo campaigns would serve when a particular regional variation of the site was loaded. Furthermore, this list was to be ever evolving and they needed a means of maintaining going forward.

Usually, our tag management system would take care of serving the right tag based on conditions the clients would create within their account. Unfortunately, given the number of regions they had to support and the requirement to be able to alter the regional lists on an ad-hoc basis, they couldn't use our productised features.


## Solution
I decided the best way of trying to resolve this was to create a custom parameter which would be to execute tag loading. That is to say, my custom code would be evaluated and depending on what is returned as the `campaignRegion` the tag manager would compare this to the rules we defined and if successful the tag would execute.

When designing the custom parameter I wanted to create it in such a way that it would allow non-technical individuals the ability to manipulating and maintain the relationship between regions and the three Crieto campaigns as they saw fit.

In my mind, the easiest way of handling this was to store the regions that they wanted in three separate arrays based on their region to campaign relationship. This would give the client an accessible list of regions they could edit as necessary for future campaigns and also meant they didn't need to edit the rest of the code within the custom parameter.

Once the arrays were created the rest of the logic would simply check the region provided to us by the client's website `tmParam.region`, and based on which array this region was found we'd return the appropraite `campaignRegion`.

Additionally, I created a step by step guide to show the end user how to use this custom parameter and how to set up their tagging rules to fire the correct tags.

## Evaluation
This piece of work was billed at 5 hours which included the creation of the user guide and testing. I managed to complete the tasks required within the billed time.

The piece was put into production shortly after being presented to the client and they maintained it going forward. While the user guide took up a large amount of time, it was a necessary evil given the team who would be using this solution included some relatively junior and non-technical members. I tried to present everything with that in mind at all time.

Looking back at the code, I am fairly happy with the split between arrays and code logic. I have noticed that the `regionLowered` variable does lack any sort of validation / error handling and could become an issue in the future if the development team on the client's side decide to pass the region to a different variable.
