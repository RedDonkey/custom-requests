(function () {
    /* Create our campaign regions and populate with the relevant countries */
    var germanyArray = ["russian federation", "germany", "france", "italy", "spain", "ukraine", "poland", "romania", "kazakhstan", "netherlands", "belgium", "greece", "czech republic", "portugal", "hungary", "sweden", "belarus", "bulgaria", "denmark", "finland", "slovakia", "norway", "ireland", "croatia", "bosnia and herzegovina", "georgia", "armenia", "lithuania", "macedonia", "slovenia", "latvia", "estonia", "lithuania", "cyprus", "luxembourg", "malta", "iceland", "monaco", "gibraltar", "nigeria", "ethiopia", "egypt", "congo", "tanzania", "kenya", "algeria", "uganda", "morocco", "ghana", "mozambique", "angola", "madagascar", "cameroon", "nigeria", "mali", "malavi", "senegal", "togo", "libya", "liberia", "namibia", "botswana", "gambia", "lesotho", "gabon", "mauritius", "swaziland", "reunion", "seychelles"];
    var americaArray = ["united states", "mexico", "canada", "guatemala", "dominican republic", "costa rica", "panama", "jamaica", "trinidad and tobago", "guadeloupe", "martinique", "bahamas", "belize", "barbados", "aruba", "antigua and barbuda", "dominica", "bermuda", "cayman islands", "greenland", "saint kitts and nevis", "saint vincent and grenadines", "turks and caicos islands", "british virgin islands", "brazil", "argentina", "venezuela", "chile", "ecuador", "bolivia", "paraguay", "uruguay"];
    var apacArray = ["brunei", "bangladesh", "bahrain", "bhutan", "brunei", "china", "hong kong", "india", "indonesia", "israel", "pakistan", "japan", "jordan", "south korea", "kuwait", "kyrgyzstan", "lebanon", "malaysia", "maldives", "mongolia", "nepal", "oman", "pakistan", "philippines", "qatar", "saudi arabia", "new zealand", "papua new guinea", "philippines", "singapore", "sri lanka", "taiwan", "thailand", "vietnam", "united arab emirates", "uzbekistan"];

    /* Takes the region parameter from the page and makes it lower case so it is usable in our check */
    var regionLowered = tmParam.region.toLowerCase();

    /* Default value to be returned if region is not found or an error is encountered, set by Criteo */
    var campaignRegion = "germany";

    /*
      Logic used to determine which Criteo campaign tag should be fired.
      There should be no need to edit below this line.
    */
    function inArray(subject, array) {
        var len = array.length;
        for (var i = 0; i < len; i++) {
            if (subject === array[i]) {
                return true;
            }
        }
        return false;
    }

    try {
        if (inArray(regionLowered, germanyArray)) {
            campaignRegion = "germany";
        } else if (inArray(regionLowered, americaArray)) {
            campaignRegion = "america";
        } else if (inArray(regionLowered, apacArray)) {
            campaignRegion = "apac";
        }
    }
    catch (err) {
        console.log("Error encountered: " + err);
    }

    return campaignRegion;
})();
