/*
 * This tag is a new template specific to Waitrose
 * (2365) custom-confirmation page
 * Vendor: AffiliateWindow
 * Template type: Confirmation page tag without product level tracking
 * Template Version: 3.0.0
 * Author: Ben Wilder
 * Modifications: Peter Avdjian
 * Date modified: 2015-11-30
 * TM ver: 3.0
 */
(function () {

	/* Declaration of template customisations */
	var custom_behaviour_1 = function () {

	};

	var custom_behaviour_2 = function () {

		/*
		 * The below the current list of Entertaining SKUs. Adding or removing items from commission calculations will
		 * require the below sku_array to be updated. Note, it must remain comma seperated.
		 */
		var sku_array = ["646311", "529044", "726045", "533041", "724969", "806334", "444210", "708481", "822336", "397626", "81763", "543744",
			"768489", "45507", "649785", "69526", "574495", "378834", "18018", "415568", "676707", "79552", "491606", "842095",
			"590174", "607810", "409434", "641722", "861330", "725958", "638497", "651176", "610034", "520815", "872761", "746466",
			"576706", "564123", "790765", "547590", "602267", "512136", "737091", "855549", "747344", "63090", "24823", "41053",
			"25790", "88493", "68755", "88394", "29018", "82829", "1920", "51519", "20287", "62590", "89287", "48300", "425108",
			"381942", "647626", "428156", "813391", "898156", "73657", "60992", "95569", "561154", "643653", "645733", "631434",
			"730325", "84341", "47587", "614517", "785044", "34799", "404042", "446446", "655194", "748796", "447231", "724114",
			"751610", "456440", "445601", "461970", "468932", "467670", "554145", "537521", "730228", "658504", "882598", "775863",
			"572870", "721165", "721165", "780180", "632243", "890873", "476875", "403229", "472454", "573716", "649318", "660196",
			"675814", "23182", "19865", "379943", "766611", "537875", "528431", "825042", "420236", "676996", "848532", "584069",
			"711836", "819821", "662890", "544720", "436104", "601892", "870449", "457178", "849775", "468128", "634098", "501192",
			"469472", "850201", "499731", "747515", "535551", "485032", "16492", "59465", "872660", "848079", "781735", "874062",
			"55586", "531334", "877329", "727028", "527944", "682859", "548114", "435783", "547173", "528759", "595713", "798223",
			"778983", "703295", "439547", "463773", "875035", "825587", "813176", "841810", "528328", "811930", "821723", "814220",
			"365689", "744992", "668399", "698355", "530199", "869414", "513785", "465404", "564514", "464652", "824501", "773339",
			"782041", "560749", "696568", "828273", "858041", "364198", "815917", "847118", "535326", "775174", "620040", "535632",
			"65206", "723416", "705768", "699714", "733999", "449263", "672387", "48554", "568093", "582948", "48525", "790355",
			"679428", "879131", "506165", "520502", "717951", "755725", "411937", "495841", "845003", "451403", "892880", "698792",
			"816876", "880249", "750310", "859335", "822566", "423483", "499441", "771968", "714445", "551143", "876076", "665955",
			"871134", "831462", "669848", "884152", "492665", "401307", "853507", "825224", "413963", "617759", "797570", "528238",
			"641603", "563158", "508034", "519698", "676220", "860698", "829177", "475888", "780169", "842679", "869488", "568367",
			"809066", "860696", "757324", "379953", "721787", "730918", "551059", "467272", "424018", "566750", "491090", "871051",
			"640585", "792016", "65791", "650979", "594598", "40344", "429229", "534443", "881076", "879028", "683538", "637931",
			"468066", "850717", "425656", "889122", "844957", "862140", "442187", "91691", "820111", "588500", "893279", "534443",
			"543642", "647937", "780569", "716784", "874392", "559380", "797051", "709074", "803890", "821072", "851159", "707529",
			"734220", "640801", "886822", "626402", "819129", "715881", "813754", "666389", "836911", "658643", "476097", "860651",
			"883467", "827533", "632820", "682550", "728451", "600422", "49703", "23371", "26204", "560031", "838192", "459194",
			"569489", "637467", "678967", "737673", "672633", "1331", "376437", "446095", "469059", "410355", "889325", "690255",
			"698859", "468009", "606128", "720135", "677364", "887325", "415066", "603681", "713251", "572902", "646724", "866856",
			"423825", "459036", "703174", "778045", "873952", "602028", "895859", "466264", "435297", "824414", "686787", "829977",
			"668827", "429189", "642744", "568772", "808452", "847683", "411939", "834256", "34352", "40715", "692320", "35223",
			"483513", "740929", "710588", "620514", "610753", "705438", "483202", "528619", "483503", "795302", "422672", "544820",
			"34749", "16935", "94303", "39454", "87192", "64113", "718334", "568211", "711011", "617818", "447902", "731288",
			"411828", "464794", "657155", "629240", "725093", "547105", "841042", "801378", "420013", "569616", "867731", "488764",
			"703153", "673518", "509773", "384595", "451212", "667536", "575862", "671374", "710687", "543036", "560357", "90557",
			"697633", "752017", "767295", "437880", "685268", "420711", "644722", "511321", "695339", "794501", "563725", "646297",
			"407993", "396958", "463196", "750638", "604493", "766671", "504886", "554348", "451583", "612258", "696349", "477402",
			"476224", "590759", "584572", "572494", "524485", "654280", "527456", "788130", "413664", "407367", "440083", "780119",
			"470958", "624379", "591494", "476839", "710016", "522065", "761972", "503685", "400066", "754802", "585281", "700727",
			"682494", "455999", "367302", "664461", "542092", "387848", "453881", "539416", "630499", "466620", "600023", "632630",
			"538769", "532400", "490792", "722092", "796804", "37649", "601489", "568476", "423214", "48534", "72597", "699044",
			"881666", "474726", "430810", "531431", "733756", "809299", "533247", "891006", "819304", "739836", "776215"];

		/*
		 * The following logic works out how many items were purchased, associates each item with its relevant price.
		 * Then checks to see if any items purchased were part of the above sku_array, before calculating the total
		 * cost for both entertaining and grocery purchases.
		 */
		var param_list_prod_price = (typeof tmParam.product_price !== 'undefined' ? tmParam.product_price.split('|') : []);
		var param_list_prod_sku = (typeof tmParam.product_sku !== 'undefined' ? tmParam.product_sku.split('|') : []);
		var param_customer_type = (typeof tmParam.customer_type !== 'undefined' ? tmParam.customer_type.toUpperCase() : '');
		var total_grocery = 0;
		var total_entertaining = 0;
		var found = [];

		for (var i = 0; i < param_list_prod_sku.length; i++) {
			for (var j = 0; j < sku_array.length; j++) {
				if (param_list_prod_sku[i] == sku_array[j]) {
					found[i] = '1';
					break;
				} else {
					found[i] = '0';
				}
			}
		}

		for (var k = 0; k < found.length; k++) {
			if (found[k] == '1') {
				total_entertaining = total_entertaining + parseFloat(param_list_prod_price[k]);
			} else {
				total_grocery = total_grocery + parseFloat(param_list_prod_price[k]);
			}
		}


		/*
		 * This section works out whether a discount voucher was used, and if so scrapes the value from the page.
		 * It then uses this value to balance the AWIN Amount and AWIN Parts totals.
		 * As scrapping is generally a no no, we're going to capture and return any errors we encounter.
		 */
		var discount = function () {
			try {
				var decodedCurrencySymbol = '-' + decodeURIComponent('%C2%A3');
				var promotionValue = jQuery("#orderdetails .details table tbody tr :contains(" + decodedCurrencySymbol + ")").html().trim().substring(2);
				return parseFloat(promotionValue);
			}
			catch (e) {
				return 'Error Encountered: ' + e;
			}
		};

		var totalParts = parseFloat(total_grocery) + parseFloat(total_entertaining);
		var multiplier = discount() / totalParts;

		total_grocery = total_grocery - (total_grocery * multiplier);
		total_entertaining = total_entertaining - (total_entertaining * multiplier);


		/*
		 * We use the following to allocate the final price to each category of goods
		 */
		if (total_entertaining > 0) {
			local_sale_parts = "Entertaining" + ":" + parseFloat(total_entertaining).toFixed(2);
		}
		if (total_grocery > 0) {
			local_sale_parts = param_customer_type + ":" + parseFloat(total_grocery).toFixed(2);
		}
		if (total_entertaining > 0 && total_grocery > 0) {
			local_sale_parts = "Entertaining" + ":" + parseFloat(total_entertaining).toFixed(2) + "|" + param_customer_type + ":" + parseFloat(total_grocery).toFixed(2);
		}

		param_order_grand_total = parseFloat(tmParam.levrev).toFixed(2);

	};

	var custom_merchant_id = {{custom_merchant_id}};
	var param_order_ref = {{param_order_ref}};
	var param_order_grand_total = {{param_order_grand_total}};
	var param_test_mode = {{param_test_mode}};
	var param_currency = {{param_currency}};
	var param_commission_group = {{param_commission_group}};
	var param_list_voucher_code = (typeof {{param_list_voucher_code}} !== 'undefined' ? {{param_list_voucher_code }}.split('|') : []);
	var param_list_custom_param = (typeof {{param_list_custom_param}} !== 'undefined' ? {{param_list_custom_param }}.split('|') : []);

	/* Calls any code present in custom_behaviour_1 */
	custom_behaviour_1();

	/* Retrieve first party cookie data */
	function readCookie(name) {
		   var nameEQ = name + "=";
		   var ca = document.cookie.split(';');
		   for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		   }
		   return null;
	}

	var local_awin_cookie = readCookie('_aw_m_' + custom_merchant_id);
	var local_awin_cookie_url = '';
	if (local_awin_cookie != null) {
	  local_awin_cookie_url += '&cks=' + local_awin_cookie;
	}

	/* Builds voucher codes */
	var local_voucher_codes = '';
	if (typeof param_list_voucher_code !== 'undefined' && param_list_voucher_code.length > 0) {
		local_voucher_codes = param_list_voucher_code.join('|');
	}

	/* Builds sale parts using order total and commission groups */
	var local_single_commission_group = 'DEFAULT';
	var local_sale_parts = local_single_commission_group + ":" + param_order_grand_total;
	if (param_commission_group) {
	  local_sale_parts = param_commission_group + ":" + param_order_grand_total;
	}

	/* Create custom parameter append */
	var local_custom_params = '';
	if (!(param_list_custom_param.length === 1 && param_list_custom_param[0].length === 0)) {
		for (var i = 0; i < param_list_custom_param.length; i++) {
			local_custom_params += '&p' + (i + 1) + '=' + param_list_custom_param[i];
		}
	}

	/* Calls any code present in custom_behaviour_2 */
	custom_behaviour_2();

	/* Creates the conversion tracking fallback image url */
	var local_img_url = 'https://www.awin1.com/sread.img?tt=ns&tv=2';
	local_img_url += "&merchant=" + custom_merchant_id + "&amount=" + param_order_grand_total + "&ref=" + param_order_ref + "&parts=" + local_sale_parts + "&vc=" + local_voucher_codes + "&testmode=" + param_test_mode + "&cr=" + param_currency + local_custom_params + local_awin_cookie_url;

	/* Creates the conversion tracking external script */
	var local_script_url = 'https://www.dwin1.com/';
	local_script_url += custom_merchant_id + ".js";

	/* Insert the conversion tracking script */
	window.AWIN = {};
	window.AWIN.Tracking = {};
	window.AWIN.Tracking.Sale = {};
	window.AWIN.Tracking.Sale.amount = param_order_grand_total;
	window.AWIN.Tracking.Sale.currency = param_currency;
	window.AWIN.Tracking.Sale.orderRef = param_order_ref;
	window.AWIN.Tracking.Sale.parts = local_sale_parts;
	window.AWIN.Tracking.Sale.voucher = local_voucher_codes;
	window.AWIN.Tracking.Sale.test = param_test_mode;

	/* Appends the conversion tracking fallback image */
	TMAN.util.appendImage(local_img_url);

	/* Append the conversion tracking external script */
	TMAN.util.appendScript(local_script_url);
})();
