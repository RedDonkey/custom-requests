## Problem
The client approached us wanting to create a means of identifying customer status (new or existing) and the type of goods being purchased so that they could pay out differing commissions based on the above.

> At the moment, we are rewarding affiliates with different rates based on customer and order type.
* New customers grocery order: £5
* Existing customers grocery order: £0.75
* Entertaining order (regardless of new or existing): 5% of transaction value
* Grocery and entertaining combined order: defaults to grocery payout based on customer type

> Upon tracking an affiliate order, we are unable to tell what type of order it is - we do pass back the customer shop number which determines the customer type so we can see this immediately. It is only when we do the validations (look up order ID on our side) we can tell what type of order it was, which allows us to assign the right commission to the order.

> Are we able to get to a position where Entertaining only orders are flagged or split out from grocery only orders upon tracking prior to the validations stage? i.e. have a tag that fires if an Entertaining line / SKU is met (if we feed this in) and if none are found to fire the groceries tag?

 > Not sure if this will complicate things further but be able to tell if an order is a combined Entertaining and grocery order. The first tag is more important, this would be a bonus.

Further to this, in subsequent communications with the client it emerged that they also wanted us to check if a discount voucher was used and subsequently work out the amount paid to affiliates would be based on the discounted value of the basket.


## Solution
To try and tackle the problem with individual tags and custom firing rules would have created to much duplication of code and the necessity to update code in multiple places. The best was of tackling the problem was to create a single tag that would contain all the logic the client wanted in one place rather than branch out into custom parameters and execution rules. In this way, if the client ever had a problem with the tag there was only one variant of it for the support team to look at and the associated campaign and executions rules would be easier to understand.

The first thing to note was that we already had a tag template within our system verified by the vendor and used across clients who wanted to implement the Awin solution. All templates came with a single `custom_behaviour` function which would be called after variable declaration and assignment to allow for users to manipulate variable values if the client was unable to pass them to us in a specific format.
Unfortunately, `custom_behaviour_1` was not a usable option to store our logic because the body of the tag would later go on to define extra variables which we were going to have to manipulate in order to correct commission allocation. Therefore, I created a secondary `custom_behaviour_2` function which would be where I would input all my logic to resolve the client issue and call the function before anything was appended to the page.

The logic worked to address the issues the client raised, firstly to determine if products were part of the Entertaining SKU or Grocery SKU, and work out the total amount of each bucket based on the products ordered.
It would then try to find and apply any appropriate discount before returning the appropriate breakdown of goods for the user's basket.

Once everything was allocated, we would pass the appropriate split to Awin's `window.AWIN.Tracking.Sale.parts` to populate the conversion tracking the vendor uses.

## Evaluation
This piece of work was escalated to me after some back and forth with other members of the team. The delays were due to a mix of people on vacation from our side, client side and vendor side, in addition to not gathering the correct requirements during initial triage. A couple phone calls with the client following escalation I was able to put together a plan and start to work in earnest on the setup. I believe this was the first time the client spoke to our technical team and was able to express what was required as opposed to communication coming through our account managers.

In terms of setup, I didn't want there to be a need to alter the code outside of the `sku_array` which acted as our source of truth as to what was an Entertaining SKU. Furthermore, it was agreed that this array would become the responsibility of the client, ensuring that should commissions not be paid properly we were not liable...of course a user guide was provided on how to make these changes, though it was attached to our helpdesk ticket which I don't have access to anymore. I've put together a brief version for this repo, but this was not the one provided.

From my perspective, I was quite happy with the logic set out and feel that any member of the team could follow it to work out what I was doing. Looking back at it, there are a couple bits that I think could have been tidied up:

* In the `for` loops I've used, I've noticed that I'm forcing the loop to calculate the length of an array each time the loop is run. This could have been made more efficient by setting the length to a variable e.g.
```
var foundLen = found.length;
for (var k = 0; k < foundLen; k++) {
    if (found[k] == '1') {
      total_entertaining = total_entertaining + parseFloat(param_list_prod_price[k]);
    }
    else {
        total_grocery = total_grocery + parseFloat(param_list_prod_price[k]);
    }
}
```

* The inability for the customer to add the value of the discount voucher to our data layer resulted in some quite nasty site scraping and manipulation to pull the value required. Despite the amount of testing that went into this, I was not 100% happy with it. I feel it is far too fragile for a production website since any change to the page source would cause this to break.

* I've tried to catch and surface errors when scrapping for discount code, but if an error was to occur we'd end up with a `NaN` value for multiplier which would break the rest of the calculations and assignments I'm doing.

* The tag itself would have been wrapped within a TagMan specific `try catch`, so should the tag error during deployment the error would be surfaced in our debug logs, at least site serving would not be affected.

* Looking at the section where I assign a value to `local_sale_parts`, I can't help but feel it is somewhat wasteful potentially rewriting the variable three times. This part of the code could be re-worked slightly to ensure `local_sale_parts` was written once.  

Unfortunately, this solution was never deployed the person who was leading this from the client side left shortly after we were happy with our testing and setup

> I wanted to send a quick note to make you aware of some team changes at $client. Unfortunately, today is my last day in the business. $client are currently recruiting for a permanent successor but in the meantime, $otherPerson on copy will temporarily take over the Affiliate Program. I have created a handover document for them so that anything outstanding or on going can be picked up.

> Thank you for your help and support, it has been great working with you.

Nothing was heard of from the client after this.
