/* Rule Id: 1258813 */
var wtCookie = Bootstrapper.Cookies.get('ensYourDetails').split('|');

Bootstrapper.AF.push(['join', 'Webtrends', 'meta', [
  ['WT.z_VehicleManufactured', wtCookie[0]],
  ['WT.z_TripType', wtCookie[1]],
  ['WT.z_TripStart', wtCookie[2]],
  ['WT.z_TripEnd', wtCookie[3]],
  ['WT.z_TripDuration', wtCookie[4]]
]]);
