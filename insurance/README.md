## Problem
I was approached by this client to see if there was anything I could do to help them serve and populate their Webtrends tagging on a particular confirmation page. Usually this would be quite straight forward and customisations to populate the tag could be made within the tag manager UI. Unfortunately, while the client had our Bootstrap implemented on all pages, the booking funnel itself was brand new and designed without any knowledge from our side. The result was a booking funnel that did not have the necessary data layer in place so the client was unable to capture any information regarding the orders that were being for their analytics suite.

After explaining why tags were not working as expecting to the client, I worked with them to understand what was required. It seemed like we needed to:

* Capture information in step 1 of the booking flow
* On the press of the `continue` button fire a tag
* Finally capture relevant information and fire a populated tag on the confirmation page

## Solution
In my mind, the premise of firing a tag on the click of a button meant that I was saving myself from the horror that would have been continuously polling the page to see if a customer changes one of the settings on the page after their initial selection.

Setting up the initial Webtrends tag to fire was easy enough, but it wouldn't be a complete solution unless I could get hold of the information to populate the tag parameters. I started looking for a way of capturing the data, despite the misunderstanding in the way our implementation works the client did have a point, customers were adding their details to the page and it was being submitted somewhere...so it must be available in some form.

I'm usually adamant to implement site scraping solutions given the fact their continued success was dependant on factors beyond our control as a tag manager. But, with little else to go on I started by looking over the pages and trying to determine if I could scrape the information I required. The page behaviour in the browser's dev tools quickly showed that site scraping wouldn't work. The way the page was designed the information we required was not exposed within the generated source. Continuing to delve into the page source I noticed a few interesting snippets:

* `ng-class="{'active': yourTripModel.TripType == 2}"`
* `ng-class="{'active': yourTripModel.TripType == 1}"`
* `ng-class="{'active': yourTripModel.Zone == 2}"`

Having never come across `ng-class` before but realising that I was looking at a codified version of our required parameters I went to Google to find out more. Turns out I stumbled across Angular! After a bit of reading and experimenting I had access to the Angular model the client was populating as their customers went about filling in their details.

`angular.element($('html')).scope();`

Time to create a "data layer" tag `RAC Step1 DL.js`, this tag would fire as quickly as possible on step 1 of the booking flow and all other tagging would be dependant on it. With the data layer in place I configured a Webtrends tag in our UI to fire on the click of the `continue` button.

Testing proved this worked correctly for me, so I went through the booking flow and configured a similar data layer tag `RAC Conf DL.js` on the confirmation page. In this case, since the data in the Angular model was present at time of page load, we could fire this tag and the relevant Webtrends tag as soon as possible to record the conversion, we were not waiting on some user interaction.

When the client tested this solution they were seeing the required data in their analytics, though they came across an issue whereby the onclick functionality I placed on the `continue` button would sometimes cause the booking flow to hang at the first step. Since I was using a productised feature and could not replicate this behaviour I had a call with the client to discuss viable alternatives.

From their side they were able to replicate, but the consistency was terribly low. Obviously they couldn't risk a solution like this causing a problem in production which meant I had to devise a new way of firing the tag.

Given the information the client wanted to capture in both steps of the journey was the same, I put forward the idea of capturing and persisting data from step 1 and serving the tag at the point of conversion on the confirmation page. The reason I didn't simply access data on the confirmation page was because the page lacked some key details about the vehicle make, model and date of manufacture of the vehicle which I thought might be useful in the future.

Due to the problems seen previously with the onclick functionality, the client didn't want me to capture any information when the `continue` button was clicked for fear of the page freezing, so I set up `On Hover Cookie.js`. The idea being, I'd still capture the same information, but I would output my data to a cookie each time the customer hovered over the `continue` button. The cookie could end up being re-written several times before the customer clicks continue but that's fine because our productised `ensSetCookie` function would handle that side of things.

Once the cookie was set the user could complete their booking journey, reach the confirmation page where instead of firing a data layer tag along with all the necessary tagging which would read our cookie and populate our tag. The specific Webtrends example used can be seen seen in `Cookie Based Tag.js`.

## Evaluation
I think this project went very well, it was a completely new experience for me and I learnt a little about Angular. I'm still far away from professing any sense of working knowledge but it was a good step I feel.

This was a piece of work I carried out alone, and few people had any visibility but I think that I have provided adequate notes within the tagging to help other members of the team who look at this work later to understand what it is that I did and why I did it.

More importantly the client was very happy that we were able to help them. Given the lockdown on their side, being unable to make changes to their site until the end of the next sprint meant they would have been left without any analytics for an entire booking journey on their site.

A couple of pieces that looking back I'm not too happy with. In the `On Hover Cookie.js` I have a `console.log` at the very end of the file, how much of a problem this is can be debated, the client site would suffer huge performance degreadation on older browsers, but technically it still works. So maybe wrapping this in `if (window.console) {}` would make more sense.

I'm also not entirely happy with the `Cookie Based Tag.js`, the solution picks items from an array to populate the relevant Webtrends parameters, in itself this looks fine. My concern is that there is a lack of note or comment explaining where the values come from or what writes `ensYourDetails` cookie in the first place, if someone decides to re-order the way the cookie is build via the `On Hover Cookie.js`, there is no validation at the tag firing stage to say if the information populating the parameters is even accurate.
