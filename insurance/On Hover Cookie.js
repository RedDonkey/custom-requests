var a = document.getElementById('continue');

/*
 * This finds where the angular model is created "ng-app" attribute
 * And create a means of accessing the model
 */

var readAng = angular.element($('html')).scope();

/*
 * Trip Model
 * This should cover everything we need to capture for vehicle info
 * If more vehicle info is needed see Vehicle Breakdown, below.
 */

var localTrip = readAng.$$childTail.yourTripModel; /* Create a local var for all trip info */
var localTripDuration = localTrip.TripDuration; /* Returns all date information relevant to your trip */
var localTripVehicle = localTrip.Vehicle; /* Returns all info including reg of your car */
var localTripType = localTrip.TripType; /* Should return 1 or 2 depending on Single or Annual */
var localTripZone = localTrip.Zone; /* Returns 1, 2, 3 depending on Zone chosen */


/*
 * Vehicle Breakdown
 * Given the way the client creates their object we can't use dot notation to access these properties
 * The below is not required at the moment (Feb 2016), but I'm futureproofing so we can add vehicle details later
 */

var localVehicle = readAng.$$childTail.vehicleInfoForm; /* Create a local var for all vehicle info */
var localVehicleMake = localVehicle["Vehicle.MakeDescription"]; /* Specifics to Make */
var localVehicleModel = localVehicle["Vehicle.ModelDescription"]; /* Specifics to Model */
var localVehicleYear = localVehicle["Vehicle.YearManufactured"]; /* Specifics to year of manufacture */

/*
 * Cookie setting logic
 */

var ensSetCookie = function(a, b) {
     Bootstrapper.Cookies.set(a, b);
};

var wtVehicleManufactured = function() {
    var date = localVehicleYear.$modelValue;
    return date.toString();
};

var wtTripType = function() {
    var tripType = localTrip.TripType;
    if (tripType === 1) {
        return 'Single Trip';
    } else {
        return 'Annual Cover';
    }
};

var wtTripStart = function() {
    return localTripDuration.StartDate;
};

var wtTripEnd = function() {
    return localTripDuration.EndDate;
};

var wtTripDuration = function() {
    if (localTrip.TripType == 1) {
        var startDate = localTripDuration.StartDate.split('/');
        var endDate = localTripDuration.EndDate.split('/');
        var startDateArranged = [];
        var endDateArranged = [];

        /* Need to Americanise the date from the page */
        startDateArranged.push(startDate[1], startDate[0], startDate[2]);
        endDateArranged.push(endDate[1], endDate[0], endDate[2]);

        var a = new Date(startDateArranged.join('/'));
        var b = new Date(endDateArranged.join('/'));
        var timeDiff = Math.abs(b.getTime() - a.getTime());
        var duration = Math.ceil(timeDiff / (1000 * 3600 * 24));
        var durationString = duration.toString();
        return durationString;
    } else {
        return '';
    }
};


a.onmouseover = function() {
    var wtcookie = wtVehicleManufactured() + '|' + wtTripType() + '|' + wtTripStart() + '|' + wtTripEnd() + '|' + wtTripDuration();
    ensSetCookie('ensYourDetails', wtcookie);

    /* Debugging purposes only. TODO: Remove when happy with cookie functionality */
    console.log(wtcookie);
};
