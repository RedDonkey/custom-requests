/*
 * This finds where the angular model is created "ng-app" attribute
 * And create a means of accessing the model
 */

var readAng = angular.element($('html')).scope();

var localConf = readAng.$$childTail.paymentSuccessModel; /* Create a local var for all conf info */
var localConfAnalytics = localConf.Analytics; /* This gives us details about the product and payment */
var localConfBasket = localConf.Basket; /* This gives us information about the trip */
var localConfPerson = localConf.Person; /* This gives the information about the person booking */
var localConfVehicle = localConf.Vehicle; /* This gives the information about the vehicle */

/* Create the DL */

Bootstrapper.dataManager.push({
    name: "WT_EuropeanBreakdownCoverConfirmationDataLayer", /* friendly name */
    id: "WT_EuropeanBreakdownCoverConfDL", /* unique name */
    data: {
        WT_VehicleManufactured: { /* unique name */
            name: "WebTrends Vehicle Manufactured Date", /* friendly name */
            get: function() {
                var date = localConfVehicle.YearManufactured;
                return date.toString();
            }
        },
        WT_TripType: { /* unique name */
            name: "WebTrends Trip Type", /* friendly name */
            get: function() {
                var tripType = localConfAnalytics.PolicyType;
                if (tripType.indexOf('Single Trip') > -1) {
                    return 'Single Trip';
                } else {
                    return 'Annual Cover';
                }
            }
        },
        WT_TripStart: { /* unique name */
            name: "WebTrends Trip Start Date", /* friendly name */
            get: function() {
                return localConf.Basket.TripStartFormatted;
            }
        },
        WT_TripEnd: { /* unique name */
            name: "WebTrends Trip End Date", /* friendly name */
            get: function() {
                try {
                    if (localConf.Basket.TripEndFormatted) {
                        return localConf.Basket.TripEndFormatted;
                    } else {
                        return '';
                    }
                } catch (e) {
                    return '';
                }
            }
        },
        WT_TripDuration: { /* unique name */
            name: "WebTrends Trip Duration", /* friendly name */
            get: function() {
                var startDate = localConf.Basket.TripStart;
                var endDate = localConf.Basket.TripEnd;

                var a = new Date(startDate.getTime());
                var b = new Date(endDate.getTime());
                var timeDiff = Math.abs(b.getTime() - a.getTime());
                var duration = Math.ceil(timeDiff / (1000 * 3600 * 24));

                return duration.toString();
            }
        }
    }
});
