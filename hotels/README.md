## Problem
The client wanted to serve Ignition One tags on their website, the idea was to help track and reduce basket abandonment. The issue that they had was twofold:

* The data layer was loaded by our Bootstrap, rather than being on-site. Not a problem in and of itself, but it was a shared single data layer across all properties. Therefore they couldn't make changes to it as easily due to other business organisations not allowing for those code changes to be pushed through.
* They wanted the tags to fire as soon as the customer had populated all the relevant fields and not wait for a user interaction such as a button click.

Calls with the vendor established the requirements, ultimately we would need to provide two different tags:

* Non booking flow pages
* Booking flow pages

The non booking flow pages would contain no data, simply make a call to the vendor system scoring individual users so that the vendor could have a reference point for those users in their system.

The booking flow tag would be the one collecting data and ultimately building the profile to use for cart abandonment.

## Solution
To get the ball rolling on this project and allow the vendor to start building user profiles I targeted the non booking flow pages initially. We were not passing any information to this tag rather we're correctly scoping the vendor template before deploying `Non booking flow.js`.

The booking flow tag on the other hand turned out to be slightly more involved that previously thought which resulted in the creation of three different tags in order to meet the needs of the vendor and client.

* Steps 12457.js
* Step 3.js
* Step 6.js

The reason for having to create three tags as opposed to a single tag came down to the inability to change the data layer tag code and the requirement to fire the tag as soon as possible after the information was available.

Since the data layer could not be changed, we couldn't add logic to capture:

* Number of bedrooms booked
* Customer email address
* Customer first and last name
* Customer opt in choice for email

Instead, the logic would need to be added within the tag code. Since these values only appeared either on steps 3 or 6 of the booking flow there would be a requirement to pass a blank string where the values didn't exist and while this could have been done, we would fail to meet the vendor requirement of firing the tags as soon as possible.

As a customer when through the booking form the vendor required their tag to be served as soon as all the relevant information for a given page existed. For steps 1, 2, 4, 5 and 7 of the booking flow the customer data was present in our data layer object so the Ignition One tag could fire as soon as our data layer tag had been served. Whereas for steps 3 and 6, we were waiting on the customer to either type in certain details, or make a selection on the page.

As far as I could see the only way to successfully fire a tag as quickly as possible and populate it as needed in each step was to split the steps out, serve the tag as a dependant of our data layer tag where we could and for steps 3 and 6 poll the page to check if the user had filled in the necessary form field before allowing the tag to fire.

The result was three tags namely:

* `Steps 12457.js`
* `Step 3.js`
* `Step 6.js`

`Steps 12457.js` was set to fire immediately, I added an extra check to ensure that it wouldn't fire on steps 3 and 6 by wrapping it in an `if` statement, but otherwise it was good to go.

`Step 3.js` firstly we would check if it was being served on the right page, and if it was we'd create an interval to ensure that a couple of document Ids were present before serving the tag. This was a necessary evil because the results page was an AJAX driven page, initially you'd see an interstitial hold screen which would define and populate `window.stepName`, the actual results (and the information we need for our tag) was only present after `document.getElementById('typeFilter')` was created.

`Step 6.js` again checking that the tag was firing on the right page was the first step, though since we were also waiting for certain elements to be filled by the user we had to ensure that they were not empty strings which resulted in an additional `if` statement checking for elements not being equal to blank strings.
The polling rate of this interval was also lower than that of `Step 3.js` because the page design was such that after the user ticked the terms and condition box they would press a submit / continue button to move on, I had to try and juggle polling rate and probability of missing the transition to the next page. 250ms seemed to be about right, but well aware that this isn't ideal!

## Evaluation
I have mixed feelings about this project. None of it was best practice as far as I'm concerned, but this was a legacy account with many hands regularly editing it. Best practice was thrown out the window a long time ago!

For me, the tagging work was minimal, the vendor tag was easy enough to import and much of the data we needed could be passed to the tag from our data layer object. The bits that were not in our data layer could be extracted due to the client's use of document Ids within their source.
The fact we designed this with `setInterval` timer is what I'm most frustrated about. We had productised features that would allow for a tag to be fired on a user interaction, mouse down, click, page transition etc... But the vendor wouldn't budge on the necessity to fire the tag as soon as possible. As a result constant timers running on the page was the only way of placating them.

Looking over the code a couple of other pieces struck me as room for improvement:

* Global y variable in vendor's `for` loop, this is present in all tags, and I should have scoped this locally to the for loop.
* Continuously looping the page is not ideal and I would rather have served the tags on a button click.
* I could have probably combined the two `document.getElementById` checks in `Step 3.js` into one to make the code a bit easier to read.
