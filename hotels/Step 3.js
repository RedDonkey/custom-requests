if (window.stepName == "results") {
  var checkElementsDL = setInterval(function() {
    /* Loop to check for elements before we try and attach */
    if (document.getElementById('results')) {
      if (document.getElementById('typeFilter') !== null) {
        var obj = Bootstrapper.dataManager.getData();
        var partyBreakdown = (obj.partyObject ? obj.partyObject : {});

        var Data = {
          locationOfStay: (obj.accomodationCode ? obj.accomodationCode : ''),
          lengthOfStay: (obj.numberOfNights ? obj.numberOfNights : ''),
          arrivalDate: (obj.arrivalDate ? obj.arrivalDate : ''),
          /* Hardcoding this value per reply in #17046 */
          numberOfLodges: '1',
          insurance: '',
          numberOfBedrooms: (jQuery('#searchParameters [name="numBeds"]').val() ? jQuery('#searchParameters [name="numBeds"]').val() : ''),
          adults: (obj.numberOfAdults ? obj.numberOfAdults : ''),
          children: (partyBreakdown['child6-16'] ? partyBreakdown['child6-16'] : ''),
          infants: (partyBreakdown['infants'] ? partyBreakdown['infants'] : ''),
          toddlers: (partyBreakdown['children2-5'] ? partyBreakdown['children2-5'] : ''),
          aChildren: (partyBreakdown['child6-16'] ? '16' : ''),
          aToddlers: (partyBreakdown['children2-5'] ? '5' : ''),
          dogs: '',
          disability: '',
          accommodationType: (obj.accomodationType ? obj.accomodationType : ''),
          price: (obj.accomodationPrice ? '£' + obj.accomodationPrice : ''),
          area: '',
          eA: '',
          fN: '',
          lN: '',
          optInForEmail: '',
          extra5BreakfastPasses: '',
          extra5Qty: '',
          extra6FireLogs: '',
          extra6Qty: '',
          extra7FamilyBbq: '',
          extra7Qty: '',
          extra8FitnessStudio: '',
          extra8Qty: '',
          extra9CharitableDonation: '',
          extra9Qty: ''
        },

        i = Data,
        d = document,
        u = encodeURIComponent,
        x = '',
        j = d.createElement('script'),
        r = d.referrer,
        s = d.getElementsByTagName('script')[0];

        j.type = 'text/javascript';
        j.async = !0;
        r && r.split(/[/:?]/)[3] != d.location.hostname && (i.ref = r);

        for (y in i) x += '&' + y + '=' + u(i[y]);
        j.src = '//uk-client.netmng.com/?aid=1324&siclientid=3289' + x;
        s.parentNode.insertBefore(j, s);

        clearInterval(checkElementsDL);

      } else {
        /*
         * DEBUG USE ONLY: console.log('not a search results page, clear interval');
         * document.getElementById('results') exists on pages which are not results pages
         * document.getElementById('typeFilter') only exists on results pages
         * checking for both will determine results page asap
         */
        clearInterval(checkElementsDL);
      }
    }
  }, 500);
}
