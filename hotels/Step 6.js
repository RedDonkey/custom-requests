if (window.stepName == "leadGuest") {
  var checkElementsDL = setInterval(function () {
    /*Loop to find when the below elements are all populated*/
      if (document.getElementById('detailsEmailAddress').value !== '' && document.getElementById('detailsFirstName').value !== '' && document.getElementById('detailsSurname').value !== '' && document.getElementById('acceptTandC').checked) {
        var obj = Bootstrapper.dataManager.getData();
        var partyBreakdown = (obj.partyObject ? obj.partyObject : {});

        var Data = {
          locationOfStay: (obj.accomodationCode ? obj.accomodationCode : ''),
          lengthOfStay: '',
          arrivalDate: (obj.arrivalDate ? obj.arrivalDate : ''),
          numberOfLodges: '',
          insurance: '',
          numberOfBedrooms: '',
          adults: (obj.numberOfAdults ? obj.numberOfAdults : ''),
          children: (partyBreakdown['child6-16'] ? partyBreakdown['child6-16'] : ''),
          infants: (partyBreakdown['infants'] ? partyBreakdown['infants'] : ''),
          toddlers: (partyBreakdown['children2-5'] ? partyBreakdown['children2-5'] : ''),
          aChildren: (partyBreakdown['child6-16'] ? '16' : ''),
          aToddlers: (partyBreakdown['children2-5'] ? '5' : ''),
          dogs: '',
          disability: '',
          accommodationType: (obj.accomodationType ? obj.accomodationType : ''),
          price: (obj.accomodationPrice ? '£' + obj.accomodationPrice : ''),
          area: '',
          eA: (document.getElementById('detailsEmailAddress').value ? document.getElementById('detailsEmailAddress').value : ''),
          fN: (document.getElementById('detailsFirstName').value ? document.getElementById('detailsFirstName').value : ''),
          lN: (document.getElementById('detailsSurname').value ? document.getElementById('detailsSurname').value : ''),
          optInForEmail: (document.getElementById('acceptTandC').checked ? 'Yes' : 'No'),
          extra5BreakfastPasses: '',
          extra5Qty: '',
          extra6FireLogs: '',
          extra6Qty: '',
          extra7FamilyBbq: '',
          extra7Qty: '',
          extra8FitnessStudio: '',
          extra8Qty: '',
          extra9CharitableDonation: '',
          extra9Qty: ''
        },

        i = Data,
        d = document,
        u = encodeURIComponent,
        x = '',
        j = d.createElement('script'),
        r = d.referrer,
        s = d.getElementsByTagName('script')[0];

        j.type = 'text/javascript';
        j.async = !0;
        r && r.split(/[/:?]/)[3] != d.location.hostname && (i.ref = r);

        for (y in i) x += '&' + y + '=' + u(i[y]);
        j.src = '//uk-client.netmng.com/?aid=1324&siclientid=3289' + x;
        s.parentNode.insertBefore(j, s);

        clearInterval(checkElementsDL);
      }
  }, 250);
}
