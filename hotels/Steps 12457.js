(function () {
    if (window.stepName !== "leadGuest" && window.stepName !== "results"){
      var obj = Bootstrapper.dataManager.getData();
      var partyBreakdown = (obj.partyObject ? obj.partyObject : {});

      var Data = {
        locationOfStay: (obj.accomodationCode ? obj.accomodationCode : ''),
        lengthOfStay :'',
        arrivalDate: (obj.arrivalDate ? obj.arrivalDate : ''),
        numberOfLodges :'',
        insurance :'',
        numberOfBedrooms :'',
        adults: (obj.numberOfAdults ? obj.numberOfAdults : ''),
        children: (partyBreakdown['child6-16'] ? partyBreakdown['child6-16'] : ''),
        infants: (partyBreakdown['infants'] ? partyBreakdown['infants'] : ''),
        toddlers: (partyBreakdown['children2-5'] ? partyBreakdown['children2-5'] : ''),
        aChildren: (partyBreakdown['child6-16'] ? '16' : ''),
        aToddlers: (partyBreakdown['children2-5'] ? '5' : ''),
        dogs :'',
        disability :'',
        accommodationType: (obj.accomodationType ? obj.accomodationType : ''),
        price: (obj.accomodationPrice ? '£' + obj.accomodationPrice : ''),
        area :'',
        eA :'',
        fN :'',
        lN :'',
        optInForEmail :'',
        extra5BreakfastPasses :'',
        extra5Qty :'',
        extra6FireLogs :'',
        extra6Qty :'',
        extra7FamilyBbq :'',
        extra7Qty :'',
        extra8FitnessStudio :'',
        extra8Qty :'',
        extra9CharitableDonation :'',
        extra9Qty :''
      },

      i = Data,
      d = document,
      u = encodeURIComponent,
      x = '',
      j = d.createElement('script'),
      r = d.referrer,
      s = d.getElementsByTagName('script')[0];

      j.type = 'text/javascript';
      j.async = !0; r && r.split(/[/:?]/)[3] != d.location.hostname && (i.ref = r);

      for (y in i) x += '&' + y + '=' + u(i[y]);
      j.src = '//uk-client.netmng.com/?aid=1324&siclientid=3289' + x;
      s.parentNode.insertBefore(j, s);
    }
})();
