/*
 * This tag has been left blank intentionally
 * It is to fire on every non-booking page
 * It will not collect data
 */

(function () {
  var Data = {
      locationOfStay: '',
      lengthOfStay: '',
      arrivalDate: '',
      numberOfLodges: '',
      insurance: '',
      numberOfBedrooms: '',
      adults: '',
      children: '',
      infants: '',
      toddlers: '',
      aChildren: '',
      aToddlers: '',
      dogs: '',
      disability: '',
      accommodationType: '',
      price: '',
      area: '',
      eA: '',
      fN: '',
      lN: '',
      optInForEmail: '',
      extra5BreakfastPasses: '',
      extra5Qty: '',
      extra6FireLogs: '',
      extra6Qty: '',
      extra7FamilyBbq: '',
      extra7Qty: '',
      extra8FitnessStudio: '',
      extra8Qty: '',
      extra9CharitableDonation: '',
      extra9Qty: ''
  },

    i = Data,
    d = document,
    u = encodeURIComponent,
    x = '',
    j = d.createElement('script'),
    r = d.referrer,
    s = d.getElementsByTagName('script')[0];

  j.type = 'text/javascript';
  j.async = !0; r && r.split(/[/:?]/)[3] != d.location.hostname && (i.ref = r);

  for (y in i) x += '&' + y + '=' + u(i[y]);
  j.src = '//uk-client.netmng.com/?aid=1324&siclientid=3289' + x;
  s.parentNode.insertBefore(j, s);
})();
